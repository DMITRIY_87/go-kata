module gitlab.com/DMITRIY_87/go-kata

go 1.20

require github.com/stretchr/testify v1.8.1

require (
	github.com/DMITRIYMALASHENKOV/greet v0.0.0-20230206150846-14a4ded7a2bd // indirect
	github.com/brianvoe/gofakeit/v6 v6.20.2 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dmitriymalashenkov/greet v0.0.0-20230201155902-467e4295f25e // indirect
	github.com/essentialkaos/translit/v2 v2.0.4 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/mattn/go-sqlite3 v1.14.16 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/ptflp/godecoder v0.0.0-20210911155149-6beba132b443 // indirect
	github.com/ptflp/gomapper v0.1.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

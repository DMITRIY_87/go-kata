package main

import (
	"errors"
	"fmt"
	"time"
)

type Post struct {
	body        string
	publishDate int64
	next        *Post
}

type Feed struct {
	length int
	start  *Post
	end    *Post
}

func (f *Feed) Append(newPost *Post) {
	if f.length == 0 {
		f.start = newPost
		f.end = newPost
	} else {
		lastPost := f.end
		lastPost.next = newPost
		f.end = newPost
	}
	f.length++
}

func (f *Feed) Remove(publishDate int64) {
	if f.length == 0 {
		panic(errors.New("Feed is empty..."))
	}
	var previousPost *Post
	currentPost := f.start

	for currentPost.publishDate != publishDate {
		if currentPost.next == nil {
			panic(errors.New("No such Post found..."))
		}

		previousPost = currentPost
		currentPost = currentPost.next
	}
	previousPost.next = currentPost.next
	f.length--
}

func (f *Feed) Insert(newPost *Post) {
	if f.length == 0 {
		f.start = newPost
	} else {
		var previousPost *Post
		currentPost := f.start

		for currentPost.publishDate < newPost.publishDate {
			previousPost = currentPost
			currentPost = previousPost.next
		}
		previousPost.next = newPost
		newPost.next = currentPost
	}
	f.length++
}

func (f *Feed) Inspect() {
	if f.length == 0 {
		fmt.Println("Feed is empty...")
	}
	fmt.Println("=================")
	fmt.Println("Feed Lenght: ", f.length)
	currentIndex := 0
	currentPost := f.start

	for currentIndex < f.length {
		fmt.Printf("Item: %v - %v\n", currentIndex, currentPost)
		currentPost = currentPost.next
		currentIndex++
	}
	fmt.Println("=================")
}

func main() {
	rightNow := time.Now().Unix()
	f := &Feed{}
	p1 := &Post{
		body:        "Lorem ipsum",
		publishDate: rightNow,
	}
	p2 := &Post{
		body:        "Dolor sit amet",
		publishDate: rightNow + 10,
	}
	p3 := &Post{
		body:        "consectetur adipiscing elit",
		publishDate: rightNow + 30,
	}
	p4 := &Post{
		body:        "sed do eiusmod tempor incididunt",
		publishDate: rightNow + 40,
	}
	f.Append(p1)
	f.Append(p2)
	f.Append(p3)
	f.Append(p4)

	newPost := &Post{
		body:        "This is a new post",
		publishDate: rightNow + 20,
	}
	f.Insert(newPost)
	f.Inspect()

	f.Remove(rightNow + 30)
	f.Inspect()
}

/* Создать структуру Post с полями:
body (тип string) - текст поста.
publishDate (тип int64) - дата публикации поста в формате Unix timestamp.
next (тип *Post) - указатель на следующий пост в потоке.

Создать структуру Feed с полями:
length (тип int) - количество постов в потоке.
start (тип *Post) - указатель на первый пост в потоке.
end (тип *Post) - указатель на последний пост в потоке.

Реализовать метод Append для структуры Feed:
Метод принимает указатель на новый пост (newPost *Post).
Метод добавляет новый пост в конец потока.

Реализовать метод Remove для структуры Feed:
Метод принимает дату публикации (publishDate int64).
Метод удаляет пост с заданной датой публикации из потока.

Реализовать метод Insert для структуры Feed:
Метод принимает указатель на новый пост (newPost *Post).
Метод вставляет новый пост в поток с учетом его даты публикации (посты должны быть отсортированы по дате публикации).

Реализовать метод Inspect для структуры Feed:
Метод выводит информацию о потоке и его постах.

Написать функцию main, которая создает несколько постов, добавляет их в поток, а затем выводит информацию о потоке с помощью метода Inspect. */

package main

import (
	"fmt"
	"sort"
)

func minimumSum(num int) int {
	var nums []int
	for num > 0 {
		nums = append(nums, num%10)
		num = num / 10
	}
	sort.Ints(nums)
	v := 0
	for i := range nums {
		if i < len(nums)/2 {
			v = v + nums[i]*10
		} else {
			v = v + nums[i]
		}
	}
	return v
}
func main() {
	fmt.Println(minimumSum(2932))
}

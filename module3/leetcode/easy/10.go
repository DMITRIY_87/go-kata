package main

import "fmt"

func shuffle(nums []int, n int) []int {
	arr := make([]int, len(nums))
	j := 0
	for i := 0; i < n; i++ {
		arr[j] = nums[i]
		j++
		arr[j] = nums[i+n]
		j++
	}
	return arr
}

func main() {
	v := []int{2, 5, 1, 3, 4, 7}
	fmt.Println(shuffle(v, 3))
}

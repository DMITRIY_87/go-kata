package main

import "fmt"

func kidsWithCandies(candies []int, extraCandies int) []bool {
	var max int
	var list []bool
	for _, v := range candies {
		if v > max {
			max = v
		}
	}
	for _, v1 := range candies {
		if v1+extraCandies >= max {
			list = append(list, true)
		} else {
			list = append(list, false)
		}
	}
	return list
}

func main() {
	v := []int{2, 3, 5, 1, 3}
	fmt.Println(kidsWithCandies(v, 3))
}

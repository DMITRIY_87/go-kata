package main

import "fmt"

func runningSum(nums []int) []int {
	arr := make([]int, len(nums))
	var sum int
	for i := range nums {
		sum = sum + nums[i]
		arr[i] = sum
	}
	return arr
}

func main() {
	v := []int{1, 2, 3, 4}
	fmt.Println(runningSum(v))
}

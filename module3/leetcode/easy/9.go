package main

import "fmt"

func finalValueAfterOperations(operations []string) int {
	sum := 0
	i := operations
	for _, j := range i {
		if j == "++X" || j == "X++" {
			sum = sum + 1
			continue
		} else if j == "--X" || j == "X--" {
			sum = sum - 1
			continue
		}
	}
	return sum
}

func main() {
	v := []string{"--X", "X++", "X++"}
	fmt.Println(finalValueAfterOperations(v))
}

package main

import "fmt"

func mostWordsFound(sentences []string) int {
	max := 0
	for i, v := range sentences {
		count := 1
		for j := range v {
			if sentences[i][j] == ' ' {
				count = count + 1
			}
			if count > max {
				max = count
			}
		}
	}
	return max
}

func main() {
	v := []string{"alice and bob love leetcode", "i think so too", "this is great thanks very much"}
	fmt.Println(mostWordsFound(v))
}

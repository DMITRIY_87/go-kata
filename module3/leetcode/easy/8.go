package main

import "fmt"

func findKthPositive(arr []int, k int) int {
	l := arr[len(arr)-1]
	nums := make([]bool, l)
	for _, n := range arr {
		nums[n-1] = true
	}
	count := 0
	for i, n := range nums {
		if !n {
			count++
		}
		if count == k {
			return i + 1
		}
	}
	return (k - count) + l
}

func main() {
	v := []int{2, 3, 4, 7, 11}
	fmt.Println(findKthPositive(v, 5))
}

package main

import "fmt"

func differenceOfSum(nums []int) int {
	a, b := 0, 0
	for _, x := range nums {
		a += x
	}
	for _, y := range nums {
		if y > 9 && y < 100 {
			b = b + y%10 + y/10
		} else if y > 99 && y < 1000 {
			b = b + (y%100)%10 + (y/10)%10 + y/100
		} else if y > 999 && y <= 2000 {
			b = b + (y%100)%10 + (y/10)%10 + y/1000 + (y/100)%10
		} else {
			b += y
		}
	}
	return a - b
}

func main() {
	v := []int{1, 15, 6, 3}
	fmt.Println(differenceOfSum(v))
}

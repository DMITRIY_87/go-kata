package main

import "fmt"

func numIdenticalPairs(nums []int) int {
	count := 0
	var i, j int
	for i = 0; i < len(nums); i++ {
		for j = i + 1; j < len(nums); j++ {
			if nums[i] == nums[j] {
				count += 1
			}
		}
	}
	return count
}

func main() {
	v := []int{1, 2, 3, 1, 1, 3}
	fmt.Println(numIdenticalPairs(v))
}

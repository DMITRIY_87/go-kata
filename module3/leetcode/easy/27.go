package main

import "fmt"

func balancedStringSplit(s string) int {
	var sum, result int
	for _, v := range s {
		if v == 'R' {
			sum += 1
		} else if v == 'L' {
			sum -= 1
		}
		if sum == 0 {
			result += 1
		}
	}
	return result
}

func main() {
	fmt.Println(balancedStringSplit("RLRRLLRLRL"))
}

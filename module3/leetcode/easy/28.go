package main

import "fmt"

func countDigits(num int) int {
	a := num
	var count = 0
	for a != 0 {
		b := a % 10
		if (num % b) == 0 {
			count++
		}
		a = a / 10
	}
	return count
}

func main() {
	fmt.Println(countDigits(1248))
}

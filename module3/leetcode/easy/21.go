package main

import "fmt"

func subtractProductAndSum(n int) int {
	var nums []int
	for n > 0 {
		nums = append(nums, n%10)
		n = n / 10
	}
	multiply := 1
	sum := 0
	for i := range nums {
		multiply = multiply * nums[i]
		sum = sum + nums[i]
	}
	return multiply - sum
}

func main() {
	fmt.Println(subtractProductAndSum(234))
}

package main

import "fmt"

func uniqueMorseRepresentations(words []string) int {
	list := []string{".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."}
	m := make(map[string]bool)
	for _, v := range words {
		var str string
		for i := 0; i < len(v); i++ {
			str += list[v[i]-'a']
		}
		m[str] = true
	}
	return len(m)
}

func main() {
	words := []string{"gin", "zen", "gig", "msg"}
	fmt.Println(uniqueMorseRepresentations(words))
}

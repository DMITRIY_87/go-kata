package main

import "fmt"

func decompressRLElist(nums []int) []int {
	var arr []int
	for i := 0; i < len(nums); i = i + 2 {
		for j := 0; j < nums[i]; j++ {
			arr = append(arr, nums[i+1])
		}
	}
	return arr
}

func main() {
	v := []int{1, 2, 3, 4}
	fmt.Println(decompressRLElist(v))
}

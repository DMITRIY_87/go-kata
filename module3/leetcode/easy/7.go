package main

import (
	"fmt"
	"strings"
)

func defangIPaddr(address string) string {
	address = strings.Replace(address, ".", "[.]", -1)
	return address
}

func main() {
	fmt.Println(defangIPaddr("1.1.1.1"))
}

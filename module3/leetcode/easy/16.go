package main

import "fmt"

func smallestEvenMultiple(n int) int {
	var i int
	for {
		i++
		if i%2 == 0 && i%n == 0 {
			return i
		}
	}
}

func main() {
	fmt.Println(smallestEvenMultiple(5))
}

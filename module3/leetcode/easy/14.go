package main

import (
	"fmt"
)

func maximumWealth(accounts [][]int) int {
	max := 0
	for _, v1 := range accounts {
		sum := 0
		for _, v2 := range v1 {
			sum = sum + v2
		}
		if sum > max {
			max = sum

		}
	}
	return max
}

func main() {
	v := [][]int{{1, 2, 3}, {3, 2, 1}}
	fmt.Println(maximumWealth(v))
}

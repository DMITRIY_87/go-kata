package main

import "fmt"

func numJewelsInStones(jewels string, stones string) int {
	m := [byte(128)]bool{}
	for i := 0; i < len(jewels); i++ {
		m[jewels[i]] = true
	}
	count := 0
	for i := 0; i < len(stones); i++ {
		if m[stones[i]] {
			count++
		}
	}
	return count
}
func main() {
	fmt.Println(numJewelsInStones("aA", "aAAbbbb"))
}

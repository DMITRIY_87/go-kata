package main

import "fmt"

func helper(n int, hashMap map[int]int) int {
	if n == 0 {
		return 0
	}
	if n == 1 || n == 2 {
		return 1
	}
	if val, ok := hashMap[n]; ok {
		return val
	}
	hashMap[n] = helper(n-1, hashMap) +
		helper(n-2, hashMap) + helper(n-3, hashMap)
	return hashMap[n]
}

func tribonacci(n int) int {
	hashMap := make(map[int]int, 0)
	return helper(n, hashMap)
}

func main() {
	fmt.Println(tribonacci(4))
}

package main

import "fmt"

func interpret(command string) string {
	var result string
	for i := 0; i < len(command); i++ {
		if command[i] == 'G' {
			result += "G"
		} else if command[i] == '(' && command[i+1] == 'a' && command[i+2] == 'l' && command[i+3] == ')' {
			result += "al"
			i += 3
		} else {
			result += "o"
			i++
		}
	}
	return result
}

func main() {
	fmt.Println(interpret("G()(al)"))
}

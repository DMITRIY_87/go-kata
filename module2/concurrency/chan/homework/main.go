package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const duration = time.Second * 30

func joinChannels(chs ...<-chan int) chan int {
	MergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					MergedCh <- id
				}
			}(ch, wg)
		}

		wg.Wait()
		close(MergedCh)
	}()

	return MergedCh
}

func generateData() chan int {
	out := make(chan int, 1000)

	go func() {
		defer close(out)
		for {
			select {
			case _, ok := <-out:
				if !ok {
					return
				}
			case out <- rand.Intn(100):
			}
		}
	}()

	return out
}

func main() {
	ticker := time.NewTicker(duration)
	done := make(chan bool, 1)

	rand.Seed(time.Now().UnixNano())
	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	out := generateData()

	go func() {
		for num := range out {
			a <- num
		}
		close(a)
	}()

	go func() {
		for num := range out {
			b <- num
		}
		close(b)
	}()

	go func() {
		for num := range out {
			c <- num
		}
		close(c)
	}()

	mainChan := joinChannels(a, b, c)

	go func() {
		for num := range mainChan {
			select {
			case <-ticker.C:
				done <- true
			case <-mainChan:
				fmt.Println(num)
			case <-done:
				return
			}
		}
	}()

	time.Sleep(duration)
	ticker.Stop()
	done <- true
}

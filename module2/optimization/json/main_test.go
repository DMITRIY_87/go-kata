package main

import "testing"

var jsonData = []byte(`
    [ {
    "id": 9223372036854769000,
    "category": {
      "id": -98006934,
      "name": "Ut in"
    },
    "name": "doggie",
    "photoUrls": [
      "nisi cupidatat",
      "ut eu cupidatat Excepteur"
    ],
    "tags": [
      {
        "id": -72310482,
        "name": "labore ullamco aliquip Duis"
      },
      {
        "id": -19804204,
        "name": "cillum nulla dolore nisi"
      }
    ],
    "status": "pending"
  },
  {
    "id": 421,
    "category": {
      "id": 0,
      "name": "dog"
    },
    "name": "tommik",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 421,
        "name": "string"
      }
    ],
    "status": "pending"
  },
  {
    "id": 53909843,
    "category": {
      "id": 9280243,
      "name": "amet officia pariatur in"
    },
    "name": "doggie",
    "photoUrls": [
      "laboris Ut exercitation",
      "aute quis"
    ],
    "tags": [
      {
        "id": -62289857,
        "name": "amet eiusmod fugiat"
      },
      {
        "id": 42955563,
        "name": "velit qui minim"
      }
    ],
    "status": "pending"
  },
  {
    "id": 83635053,
    "category": {
      "id": 47434307,
      "name": "proident dolor"
    },
    "name": "doggie",
    "photoUrls": [
      "velit mollit ut ea aliqua",
      "consectetur sed"
    ],
    "tags": [
      {
        "id": 5701771,
        "name": "reprehenderit magna amet non exercitation"
      },
      {
        "id": -13012819,
        "name": "repr"
      }
    ],
    "status": "pending"
  }
    ]`)

func BenchmarkStandardJson(b *testing.B) {
	var (
		pets Pets
		err  error
		data []byte
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPets(jsonData)
		if err != nil {
			panic(err)
		}
		data, err = pets.Marshal()
		if err != nil {
			panic(err)
		}
		_ = data
	}
}

func BenchmarkJsonIter(b *testing.B) {
	var (
		pets Pets
		err  error
		data []byte
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPets2(jsonData)
		if err != nil {
			panic(err)
		}
		data, err = pets.Marshal2()
		if err != nil {
			panic(err)
		}
		_ = data
	}
}

/*
cpu: Intel(R) Core(TM) i7-7820HQ CPU @ 2.90GHz
BenchmarkStandardJson-8            12878             89131 ns/op
BenchmarkJsonIter-8                33097             39831 ns/op
*/

package main

import "testing"

func Benchmark_SanitizeText(b *testing.B) {
	for i := 0; i < b.N; i++ {
		for i := range data {
			SanitizeText(data[i])
		}
	}

}

func Benchmark_SanitizeText2(b *testing.B) {
	ft := NewFilterText()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for i := range data {
			ft.SanitizeText2(data[i])
		}
	}
	/*
	   cpu: Intel(R) Core(TM) i7-7820HQ CPU @ 2.90GHz
	   Benchmark_SanitizeText-8             213           5422511 ns/op
	   Benchmark_SanitizeText2-8            348           3480049 ns/op
	*/

}

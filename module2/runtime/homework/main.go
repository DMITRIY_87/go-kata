package main

import (
	"fmt"
	"runtime"
)

func main() {
	//runtime.GOMAXPROCS(runtime.NumCPU())
	fmt.Println("i can manage")
	go func() {
		fmt.Println("goroutines in Golang!")
	}()
	runtime.Gosched()
	//runtime.Goexit() /* требуется ли остановить горутину, что бы не выводить последний принт? */
	fmt.Println("and its awesome!")
}

package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMultiply(t *testing.T) {
	type testCase struct {
		a        float64
		b        float64
		expected float64
	}

	testCases := []testCase{
		{a: 3, b: 3, expected: 9},
		{a: 8, b: 4, expected: 32},
		{a: 15, b: 7, expected: 105},
		{a: 8, b: 8, expected: 64},
		{a: 9, b: 9, expected: 81},
		{a: 1.5, b: 8.5, expected: 12.75},
		{a: 4, b: 4, expected: 16},
		{a: 101, b: 3, expected: 303},
	}
	for _, test := range testCases {
		expectedResult := test.expected
		receivedResult := Multiply(test.a, test.b)
		assert.Equal(t, expectedResult, receivedResult)
	}
}

func TestSum(t *testing.T) {
	type testCase struct {
		a        float64
		b        float64
		expected float64
	}

	testCases := []testCase{
		{a: 4, b: 3, expected: 7},
		{a: 8, b: 4, expected: 12},
		{a: 15, b: 7, expected: 22},
		{a: 8, b: 18, expected: 26},
		{a: 9, b: 9, expected: 18},
		{a: 1.5, b: 8.5, expected: 10},
		{a: 4, b: 14, expected: 18},
		{a: 101, b: 3, expected: 104},
	}
	for _, test := range testCases {
		expectedResult := test.expected
		receivedResult := Sum(test.a, test.b)
		assert.Equal(t, expectedResult, receivedResult)
	}
}

func TestDivide(t *testing.T) {
	type testCase struct {
		a        float64
		b        float64
		expected float64
	}

	testCases := []testCase{
		{a: 9, b: 3, expected: 3},
		{a: 8, b: 4, expected: 2},
		{a: 14, b: 7, expected: 2},
		{a: 8, b: 8, expected: 1},
		{a: 81, b: 9, expected: 9},
		{a: 1.5, b: 0.5, expected: 3},
		{a: 4, b: 2, expected: 2},
		{a: 102, b: 3, expected: 34},
	}
	for _, test := range testCases {
		expectedResult := test.expected
		receivedResult := Divide(test.a, test.b)
		assert.Equal(t, expectedResult, receivedResult)
	}
}

func TestAverage(t *testing.T) {
	type testCase struct {
		a        float64
		b        float64
		expected float64
	}

	testCases := []testCase{
		{a: 9, b: 3, expected: 6},
		{a: 8, b: 8, expected: 8},
		{a: 14, b: 7, expected: 10.5},
		{a: 8, b: 18, expected: 13},
		{a: 81, b: 9, expected: 45},
		{a: 1.5, b: 0.5, expected: 1},
		{a: 4, b: 2, expected: 3},
		{a: 102, b: 3, expected: 52.5},
	}
	for _, test := range testCases {
		expectedResult := test.expected
		receivedResult := Average(test.a, test.b)
		assert.Equal(t, expectedResult, receivedResult)
	}
}

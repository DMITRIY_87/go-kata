package main

import (
	"fmt"

	"gitlab.com/DMITRIY_87/go-kata/module2/functions/defer/homework/solution"
)

func main() {

	fmt.Println(solution.ExecuteMergeDictsJob(&solution.MergeDictsJob{})) // &MergeDictsJob{IsFinished: true}, "at least 2 dictionaries are required"
	// ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"},nil}}) // &MergeDictsJob{IsFinished: true, Dicts: []map[string]string{{"a": "b"},nil}}, "nil dictionary"
	// ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"},{"b": "c"}}}) // &MergeDictsJob{IsFinished: true, Dicts: []map[string]string{{"a": "b", "b": "c"}}}, nil
	fmt.Println(solution.ExecuteMergeDictsJob(&solution.MergeDictsJob{Dicts: []map[string]string{{"a": "b"}, nil}}))
	fmt.Println(solution.ExecuteMergeDictsJob(&solution.MergeDictsJob{Dicts: []map[string]string{{"a": "b"}, {"b": "c"}}}))
}

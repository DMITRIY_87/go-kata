package solution

import (
	"errors"
)

type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

// errors
var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
)

func ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error) {
	job.Merged = map[string]string{}

	for _, m := range job.Dicts {
		if m != nil {
			for key, value := range m {
				job.Merged[key] = value
			}
			job.IsFinished = true
		} else if m == nil {
			return job, errNilDict
		}
	}
	if len(job.Dicts) < 2 {
		job.IsFinished = true
		return job, errNotEnoughDicts
	}
	return job, nil
}

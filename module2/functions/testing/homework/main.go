package main

import (
	"fmt"
	"unicode"
)

func Greet(name string) string {
	for _, r := range name {
		if unicode.Is(unicode.Cyrillic, r) {
			return fmt.Sprintf("Привет %s, добро пожаловать!", name)
		}
	}
	return fmt.Sprintf("Hello %s, you welcome!", name)
}

func main() {
	fmt.Println(Greet("Денис"))
	fmt.Println(Greet("Alex"))
}

package main

import "testing"

var (
	users    = genUsers()
	products = genProducts()
)

func BenchmarkSample(b *testing.B) {
	for i := 0; i < b.N; i++ {
		MapUserProducts(users, products)
	}
}

func BenchmarkSample2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		MapUserProducts2(users, products)
	}
}

/* сломал голову, выдавал результат:
cpu: Intel(R) Core(TM) i7-7820HQ CPU @ 2.90GHz
BenchmarkSample-8            248           4760087 ns/op
BenchmarkSample2-8           253           4762345 ns/op

из-за того что передавал не инизиализированное значчения в бенчмарк, а  MapUserProducts(genUsers(), genProducts())......
Понял ошибку, но не понял почему это так работало.

*/

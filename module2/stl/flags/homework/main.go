package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
)

type Config struct {
	AppName    string
	Production bool
}

func main() {
	conf := Config{}
	c := flag.String("c", "/Users/admin/go/src/gitlab.com/DMITRIY_87/go-kata/module2/stl/flags/homework/config.json", "Specify the configuration file")
	flag.Parse()
	file, err := os.Open(*c)
	if err != nil {
		log.Fatal("can't open config file: ", err)
	}
	defer file.Close()

	decoder := json.NewDecoder(file)
	conf = Config{}
	err = decoder.Decode(&conf)
	if err != nil {
		log.Fatal("can't decode config JSON: ", err)
	}

	fmt.Println(conf)

}

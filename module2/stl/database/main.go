package main

import (
	"database/sql"
	"fmt"
)

func main() {
	database, _ := sql.Open("sqlite3", "./gopher.db")

	var statement, _ = database.Prepare("CREATE TABLE IF NOT EXISTS people (id INTEGER PRIMARY KEY, firstname TEXT, lastname TEXT)")
	_, _ = statement.Exec()
	//defer database.Close()

	statement, _ = database.Prepare("INSERT INTO people (firstname, lastname) VALUES (?, ?)")
	_, _ = statement.Exec("Lorem", "Ipsum")
	//defer database.Close()

	rows, _ := database.Query("SELECT id, firstname FROM people")
	var id int
	var firstname string
	//var lastname string

	for rows.Next() {
		_ = rows.Scan(&id, &firstname)
		fmt.Printf("%d: %s\n", id, firstname)
	}
}

package main

import (
	"bufio"
	"fmt"
	"os"

	"github.com/essentialkaos/translit/v2"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {

	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Enter your  name: ")

	name, _ := reader.ReadString('\n')
	fmt.Printf("Hello %s\n", name)

	data, err := os.ReadFile("./module2/stl/files/write/example.txt")
	check(err)

	dataTranslit := translit.EncodeToPCGN(string(data))

	f, err := os.Create("./module2/stl/files/write/example.processed.txt")
	check(err)

	defer f.Close()

	_, err = f.WriteString(dataTranslit)
	check(err)

}

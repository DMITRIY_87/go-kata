package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
)

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}

	var buf bytes.Buffer
	for _, p := range data {
		_, err := buf.WriteString(p)
		buf.WriteString("\n")
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

	}
	//fmt.Println(buf.String())
	fileWrite, err := os.Create("./module2/stl/io/homework/example.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer fileWrite.Close()

	if _, err := io.WriteString(fileWrite, buf.String()); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fileRead, err := os.Open("./module2/stl/io/homework/example.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer fileRead.Close()

	var buffer bytes.Buffer
	if _, err = buffer.ReadFrom(fileRead); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Println(buffer.String()) // выведите данные из буфера buffer.String()
	// у вас все получится!
}

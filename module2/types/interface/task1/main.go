package main

import (
	"fmt"
)

type MyInterface interface{}

func main() {
	var n *int
	fmt.Println(n == nil)
	test(n)

}

func test(r interface{}) {
	switch value := r.(type) {
	case *int:
		if value == nil {
			fmt.Println("Success!")
		}

	}
}

package main

import "fmt"

func main() {
	var numberInt int = 3
	//numberInt = 3
	var numberFloat float32 = float32(numberInt)
	//numberFloat = float32(numberInt)
	fmt.Printf("тип: %T, значение: %v \n", numberFloat, numberFloat)

	var numberUint uint = 34
	//numberUint = 34
	var numberFloat64 float64 = float64(numberUint)
	//numberFloat64 = float64(numberUint)
	fmt.Printf("тип: %T, значение: %v", numberFloat64, numberFloat64)
}

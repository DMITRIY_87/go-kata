package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println("size is:", unsafe.Sizeof(n), "bytes")
	typeUint()
	typeInt()
}

func typeInt() {
	fmt.Println("=== START type int ===")
	var uintNumber uint8 = 1 << 7
	var min = int8(uintNumber)
	uintNumber--
	var max = int8(uintNumber)
	fmt.Println("int8 min value:", min, "int8 max value:", max, "size:", unsafe.Sizeof(min), "bytes")
	var uintNumber16 uint16 = 1 << 15
	var min16 = int16(uintNumber16)
	uintNumber16--
	var max16 = int16(uintNumber16)
	fmt.Println("int16 min value:", min16, "int16 max value:", max16, "size:", unsafe.Sizeof(min16), "bytes")
	var uintNumber32 uint32 = 1 << 31
	var min32 = int32(uintNumber32)
	uintNumber32--
	var max32 = int32(uintNumber32)
	fmt.Println("int32 min value:", min32, "int32 max value:", max32, "size:", unsafe.Sizeof(min32), "bytes")
	var uintNumber64 uint64 = 1 << 63
	var min64 = int64(uintNumber64)
	uintNumber64--
	var max64 = int64(uintNumber64)
	fmt.Println("int64 min value:", min64, "int64 max value:", max64, "size:", unsafe.Sizeof(min64), "bytes")
	fmt.Println("=== END type int ===")
}

func typeUint() {
	fmt.Println("=== START type uint ===")
	var numberUint8 uint8 = 1 << 1
	fmt.Println("left shift uint8:", numberUint8, "size:", unsafe.Sizeof(numberUint8), "bytes")
	numberUint8 = (1 << 8) - 1
	fmt.Println("uint8 max value:", numberUint8, "size:", unsafe.Sizeof(numberUint8), "bytes")
	var numberUint16 uint16 = (1 << 16) - 1
	fmt.Println("uint16 max value:", numberUint16, "size:", unsafe.Sizeof(numberUint16), "bytes")
	var numberUint32 uint32 = (1 << 32) - 1
	fmt.Println("uint32 max value:", numberUint32, "size:", unsafe.Sizeof(numberUint32), "bytes")
	var numberUint64 uint64 = (1 << 64) - 1
	fmt.Println("uint64 max value:", numberUint64, "size:", unsafe.Sizeof(numberUint64), "bytes")
	fmt.Println("=== END type uint ===")
}

package main

import "fmt"

func main() {
	var uintNumber uint8 = 1 << 7
	var from = int8(uintNumber)
	uintNumber--
	var to = int8(uintNumber)
	fmt.Println(from, to)
}

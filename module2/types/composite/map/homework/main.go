package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/GoesToEleven/GolangTraining",
			Stars: 8721,
		},
		{
			Name:  "https://github.com/golang/go",
			Stars: 107760,
		},
		{
			Name:  "https://github.com/cornelk/hashmap",
			Stars: 1437,
		},
		{
			Name:  "https://github.com/geektutu/7days-golang",
			Stars: 12323,
		},
		{
			Name:  "https://github.com/SimonWaldherr/golang-examples",
			Stars: 1405,
		},
		{
			Name:  "https://github.com/a8m/golang-cheat-sheet",
			Stars: 7233,
		},
		{
			Name:  "https://github.com/lightvector/KataGo",
			Stars: 2159,
		},
		{
			Name:  "https://github.com/CachetHQ/Docker",
			Stars: 387,
		},
		{
			Name:  "https://github.com/golang/tools",
			Stars: 6623,
		},
		{
			Name:  "https://github.com/json-iterator/go",
			Stars: 11727,
		},
		{
			Name:  "https://github.com/exercism/go",
			Stars: 787,
		},
		{
			Name:  "https://github.com/golang-standards/project-layout",
			Stars: 37243,
		},
	}

	mp := make(map[string]Project)
	for i := range projects {
		mp[projects[i].Name] = projects[i]
	}

	for k, v := range mp {
		fmt.Println("key:", k, "Stars:", v.Stars, "Name:", v.Name)
	}
}
